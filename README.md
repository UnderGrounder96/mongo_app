# mongo_app

This mongo_app is just an excuse for me to learn K8s.

## Getting started

This program was created under Enterprise Linux 7.9 using K8s' Mongo Express and MongoDB images.

## Prerequisites

[Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) and [minikube](https://minikube.sigs.k8s.io/docs/start/).

## Deployment

Please execute the following commands:

```bash
kubectl apply -f src/mongo-secret.yml
kubectl apply -f src/mongo-db.yml
kubectl apply -f src/mongo-configmap.yml
kubectl apply -f src/mongo-express.yml
minikube service mongo-express-service --url
```

After that an IP would be revealed that exposes the entire application.

Additionally one could also implement the `nginx-ingress` by executing:

```bash
minikube addons enable ingress
kubectl apply -f src/ngnix-ingress.yml
echo "192.168.49.2 mongo-app.com" | sudo tee -a /etc/hosts
```

*WARNING:* The IP provided here comes from `minikube service ...`, please reassure it's the same for you.

## Author

Lucio Afonso

## License

This project is licensed under the GPL License - see the LICENSE file for details
